<?php

require_once '../database/Connection.php';

abstract class AbsExec01 extends Connection{

    abstract public function insert();

    abstract public function findAll();

    abstract public function delete();

}
