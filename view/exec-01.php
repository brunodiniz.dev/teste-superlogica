<?php
require_once '../controllers/Exec01.php';

$exercio01 = new Exec01();

if($_POST['function'] === 'delete'){
    $exercio01->setId($_POST['id']);
    $exercio01->delete();
}

if($_GET['findAll']){
    $exercio01->findAll();
}

if($_POST['function'] === 'insert'){

    if($exercio01->validation() === true){
        $exercio01->insert();
    }
}
