<?php
require_once '../controllers/Exec02.php';
$title = "Exercício 02 - Superlógica";
include 'includes/head.php';
?>

<div class="row">
    <div class="col-md-12">
        <header>
            Exercício 02 - Superlógica
        </header>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-1">
        <div class="exec02">
            <?php
            $exercio02 = new Exec02();
            $exercio02->index();
            ?>
        </div>
    </div>

    <div class="col-md-2">
        <a href="../index.php" class="btn btn-primary">Tela inicial</a>
    </div>
</div>

<?php
include 'includes/footer.php';
?>
