<?php
$title = "Exercício 01 - Superlógica";
include 'includes/head.php';
?>

<div class="row">
    <div class="col-md-12">
        <header>
            Exercício 01 - Superlógica
        </header>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="titulo">
            <h5>Formulário de Cadastro</h5>
            <hr>
        </div>

        <form method="post" action="exec-01.php">
            <div class="form-group">
                <label for="name">Nome completo:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="userName">Nome de login:</label>
                <input type="text" class="form-control" id="userName" name="userName">
            </div>
            <div class="form-group">
                <label for="zipCode">CEP</label>
                <input type="text" class="form-control" id="zipCode" name="zipCode">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailInfo">
                <small id="emailInfo" class="form-text text-muted">Obrigatório inserir um e-mail válido</small>
            </div>
            <div class="form-group">
                <label for="password">Senha:</label>
                <input type="password" class="form-control" id="password" name="password" aria-describedby="senhaInfo">
                <small id="senhaInfo" class="form-text text-muted">8 caracteres mínimo, contendo pelo menos 1 letra
                    e 1 número.</small>
            </div>
            <button id="btnCadastrar" type="button" class="btn btn-primary">Cadastrar</button>
        </form>
    </div>

    <div class="col-md-8">
        <div class="titulo">
            <h5>Lista de cadastro</h5>
            <hr>
        </div>

        <table class="table table-striped center-header table table-bordered dt-responsive">
            <thead>
            <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Login</th>
                <th>CEP</th>
                <th>E-mail</th>
                <th>Data de criação</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody id="bodyTable">
            </tbody>
        </table>

        <button id="btnAtualizarTabela" class="btn btn-info">Atualizar Tabela</button>
        <a href="../index.php" class="btn btn-primary">Tela inicial</a>
    </div>
</div>

<?php
include 'includes/footer.php';
?>
