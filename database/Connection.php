<?php

require_once '../config/Config.php';

class Connection{

    private static $instance;

    public static function getInstance(){

        if(!isset(self::$instance)){
            try{

                self::$instance = new PDO('mysql:host=' . HOST . ';dbname=' . DATABASE, USER, PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

                return self::$instance;
            }catch(PDOException $e){

                echo $e->getMessage();

            }
        }
    }

    public static function prepare($query){
        return self::getInstance()->prepare($query);
    }

}
