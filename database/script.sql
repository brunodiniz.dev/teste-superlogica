CREATE DATABASE IF NOT EXISTS superlogica CHARACTER SET utf8 COLLATE utf8_general_ci;

USE superlogica;

CREATE TABLE IF NOT EXISTS status (

	status_id INT NOT NULL,
	status_descricao VARCHAR(60) NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (status_id)

);

INSERT INTO superlogica.status(status_id, status_descricao) VALUES
(1, 'Ativo'),
(2, 'Inativo'),
(3, 'Excluído');

CREATE TABLE IF NOT EXISTS exercicio01 (

	exec01_id INT NOT NULL AUTO_INCREMENT,
	exec01_name VARCHAR(60) NOT NULL,
	exec01_username VARCHAR(60) NOT NULL,
	exec01_zipcode INT NOT NULL,
	exec01_email VARCHAR(80) NOT NULL,
	exec01_password TEXT NOT NULL,
	status_id INT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (exec01_id),
	FOREIGN KEY (status_id) REFERENCES status(status_id)

);

CREATE TABLE IF NOT EXISTS usuarios (

	usuario_id INT NOT NULL,
	usuario_cpf BIGINT NOT NULL,
	usuario_nome VARCHAR(60) NOT NULL,
	status_id INT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (usuario_id),
	FOREIGN KEY (status_id) REFERENCES status(status_id)

);

CREATE TABLE IF NOT EXISTS infos (

	info_id INT NOT NULL,
	info_cpf BIGINT NOT NULL,
	info_genero VARCHAR(1) NOT NULL,
	info_ano_nascimento INT NOT NULL,
	status_id INT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (info_id),
	FOREIGN KEY (status_id) REFERENCES status(status_id)

);

INSERT INTO superlogica.usuarios(usuario_id, usuario_cpf, usuario_nome, status_id) VALUES
(1, 16798125050, 'Luke Skywalker', 1),
(2, 59875804045, 'Bruce Wayne', 1),
(3, 04707649025, 'Diane Prince', 1),
(4, 21142450040, 'Bruce Banner', 1),
(5, 83257946074, 'Harley Quinn', 1),
(6, 07583509025, 'Peter Parker', 1);

INSERT INTO superlogica.infos(info_id, info_cpf, info_genero, info_ano_nascimento, status_id) VALUES
(1, 16798125050, 'M', 1976, 1),
(2, 59875804045, 'M', 1960, 1),
(3, 04707649025, 'F', 1988, 1),
(4, 21142450040, 'M', 1954, 1),
(5, 83257946074, 'F', 1970, 1),
(6, 07583509025, 'M', 1972, 1);

SELECT
	CONCAT(superlogica.usuarios.usuario_nome, ' - ', superlogica.infos.info_genero) AS 'usuario',
	IF ((YEAR(NOW()) - superlogica.infos.info_ano_nascimento) > 50, 'SIM', 'NÃO') AS 'maior_50_anos'
FROM
	superlogica.usuarios
INNER JOIN superlogica.infos ON
	superlogica.infos.info_cpf = superlogica.usuarios.usuario_cpf
	WHERE superlogica.usuarios.status_id = 1 AND superlogica.infos.info_genero = 'M'
ORDER BY superlogica.usuarios.usuario_cpf ASC
LIMIT 3;
