<?php

require_once '../helpers/Helpers.php';
require_once '../abstracts/AbsExec01.php';

class Exec01 extends AbsExec01 {

    private $id;
    private $name;
    private $userName;
    private $zipCode;
    private $email;
    private $password;
    private $status;
    private $db;

    protected $database = 'superlogica';
    protected $table = 'exercicio01';

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getUserName() {
        return $this->userName;
    }

    public function setUserName($userName) {
        $this->userName = $userName;
    }

    public function getZipCode() {
        return $this->zipCode;
    }

    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function validation(){

        $this->setName($_POST['name']);
        $this->setUserName($_POST['userName']);
        $this->setZipCode($_POST['zipCode']);
        $this->setEmail($_POST['email']);
        $this->setPassword($_POST['password']);

        try{

            //Validação dos inputs dos form
            switch ($this->getName()){
                case strlen($this->getName()) < '3':
                    throw new Exception('É necessário no minímo 3 letras no campo nome.');

                case '':
                    throw new Exception('O campo nome não pode estar em branco.');

                default:
                    break;
            }

            if(empty($this->getUserName())){
                $this->setUserName(strtolower(str_replace(" ", "", $this->getName())));
            }

            if(!checkCep($this->getZipCode())){
                throw new Exception('CEP inválido!');
            }else{
                $this->setZipCode(str_replace("-", "", $this->getZipCode()));
                (int) $this->getZipCode();
            }

            if(!checkEmail($this->getEmail())){
                throw new Exception('E-mail inválido!');
            }

            if(!checkPassword($this->getPassword())){
                throw new Exception('A senha deve conter no mínimo 8 caracteres, uma letra e um número!');
            }
            //Validação dos inputs dos form

            return true;
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function insert() {

        try {
            $query = "INSERT INTO $this->database.$this->table (exec01_name, exec01_username, exec01_zipcode, exec01_email, exec01_password, status_id) VALUES (:name, :username, :zipcode, :email, :password, :status_id);";

            $this->password = trim(password_hash($this->getPassword(), PASSWORD_DEFAULT));
            $this->status = 1;

            $this->db = Connection::getInstance();

            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':name', $this->name);
            $stmt->bindParam(':username', $this->userName);
            $stmt->bindParam(':zipcode', $this->zipCode);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);
            $stmt->bindParam(':status_id', $this->status);

            if($stmt->execute()){
                echo $this->db->lastInsertId();
            }

        } catch (PDOExecption $e) {
            echo "Erro: " . $e->getMessage() . "</br>";
        }
    }

    public function findAll() {

        try {
            $query = "SELECT * FROM $this->database.$this->table WHERE $this->table.status_id = 1;";
            $stmt = Connection::prepare($query);
            $stmt->execute();
            echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOExecption $e) {
            echo "Erro!: " . $e->getMessage() . "</br>";
        }
    }

    public function delete() {

        try {

            $this->status = 3;

            $query = "UPDATE $this->database.$this->table SET status_id = :status_id WHERE $this->database.$this->table.exec01_id = :id;";
            $stmt = Connection::prepare($query);
            $stmt->execute(array(
                ':status_id' => $this->status,
                ':id' => (int) $this->id
            ));

            echo $stmt->rowCount();
        } catch (PDOException $e) {
            echo 'Erro: ' . $e->getMessage();
        }
    }

}
