<?php

require_once '../helpers/Helpers.php';
require_once '../abstracts/AbsExec01.php';

class Exec02{

    public function index(){

        $array = array();
        $strArray = "";

        echo nl2br("01 - Crie um array: " . json_encode($array) . "\n\n");

        for($i = 1; $i <= 7; $i++){
            array_push($array, mt_rand(1, 100));
        }

        echo nl2br("02 - Popule este array com 7 números: " . json_encode($array) . "\n\n");
        echo nl2br("03 - Imprima o número da posição 3 do array: " . $array[3] . "\n\n");

        foreach ($array as $key => $value){
            $strArray .= $value . ', ';
        }
        echo nl2br("04 - Crie uma variável com todas as posições do array no formato de string separado por
vírgula: " . substr($strArray, 0, -2) . "\n\n");

        $strFormated = explode(',', str_replace(" ", "", substr($strArray, 0, -2)));
        $array05 = array();
        foreach($strFormated as $arrNumber){
            array_push($array05, intval($arrNumber));
        }
        echo nl2br("05 - Crie um novo array a partir da variável no formato de string que foi criada e destrua o
array anterior: " . json_encode($array05) . " -- ");

        if(isset($array)){
            unset($array);
            echo nl2br("O primeiro array foi destruído! \n\n");
        }

        if(in_array("14", $array05)){
            echo nl2br("06 - Crie uma condição para verificar se existe o valor 14 no array: Existe! \n\n");
        }else{
            echo nl2br("06 - Crie uma condição para verificar se existe o valor 14 no array: Não existe! \n\n");
        }


        echo nl2br("07 - Faça uma busca em cada posição. Se o número da posição atual for menor que o da posição anterior (valor anterior que não foi excluído do array ainda), exclua esta posição: \n\n");
        $anterior = "";
        foreach($array05 as $index => $val){

            if(!$index == "0"){
                if($val < $anterior){
                    unset($array05[$index]);
                    echo nl2br("A posição " .$index. " foi excluída! \n\n");
                }else{
                    $anterior = $val;
                }
                echo nl2br(json_encode($array05) . "\n");
            }else{
                $anterior = $val;
                echo nl2br(json_encode($array05) . "\n");
            }
        }
        echo nl2br("\n\n");

        echo nl2br("08 - Remova a última posição deste array: " .json_encode($array05). " -- Foi removida a posíção " .array_pop($array05). " -- veja: " .json_encode($array05). "\n\n");

        echo nl2br("09 - Conte quantos elementos tem neste array: O array05 possui " .count($array05). " elementos! \n\n");

        echo nl2br("10 - Inverta as posições deste array: " .json_encode(array_reverse($array05)). "\n\n");
    }

}
