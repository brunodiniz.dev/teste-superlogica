<?php

require_once '../helpers/Helpers.php';
require_once '../abstracts/AbsExec03.php';

class Exec03 extends AbsExec03 {

    private $id;
    private $cpf;
    private $nome;
    private $genero;
    private $ano_nascimento;

    protected $database = 'superlogica';

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCpf() {
        return $this->name;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getGenero() {
        return $this->genero;
    }

    public function setGenero($genero) {
        $this->genero = $genero;
    }

    public function getAno_nascimento() {
        return $this->ano_nascimento;
    }

    public function setAno_nascimento($ano_nascimento) {
        $this->ano_nascimento = $ano_nascimento;
    }

    public function findAll() {

        try {
            $query = "SELECT
                        CONCAT($this->database.usuarios.usuario_nome, ' - ', $this->database.infos.info_genero) AS 'usuario',
                        IF ((YEAR(NOW()) - $this->database.infos.info_ano_nascimento) > 50, 'SIM', 'NÃO') AS 'maior_50_anos'
                    FROM
                        $this->database.usuarios
                    INNER JOIN $this->database.infos ON
                        $this->database.infos.info_cpf = $this->database.usuarios.usuario_cpf
                        WHERE $this->database.usuarios.status_id = 1 AND $this->database.infos.info_genero = 'M'
                    ORDER BY $this->database.usuarios.usuario_cpf ASC
                    LIMIT 3;";
            $stmt = Connection::prepare($query);
            $stmt->execute();
            echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOExecption $e) {
            echo "Erro!: " . $e->getMessage() . "</br>";
        }
    }
}
