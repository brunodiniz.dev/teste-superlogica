$(document).ready(function() {

    $("input[name='zipCode']").mask('00000-000');

    function limpaForm() {

        $("input[name='name']").val('');
        $("input[name='userName']").val('');
        $("input[name='zipCode']").val('');
        $("input[name='email']").val('');
        $("input[name='password']").val('');
    }

    function get_cadastros(){

        $.ajax('/view/exec-01.php', {

            method: 'GET',
            data: {findAll: 1},
            dataType: 'JSON',
            beforeSend: function(){
                $('#bodyTable').html('');
            },
            success: function(response){

                var cadastros;

                $.each(response, function(index, value){
                    var data = new Date(value.created_at);

                    var string = value.exec01_zipcode;
                    var metade = Math.floor((string.length + 1) / 2);
                    var resultado = string.substr(0,metade)+"-"+string.substr(metade);

                    cadastros += "<tr>";
                    cadastros += "<td>"+value.exec01_id+"</td>";
                    cadastros += "<td>"+value.exec01_name+"</td>";
                    cadastros += "<td>"+value.exec01_username+"</td>";
                    cadastros += "<td>"+resultado+"</td>";
                    cadastros += "<td>"+value.exec01_email+"</td>";
                    cadastros += "<td>"+data.toLocaleString('pt-BR', {timezone: 'UTC'})+"</td>";
                    cadastros += "<td><button id='btnDelete' class='btn btn-danger' data-id="+value.exec01_id+"><i class='fas fa-trash-alt'></i></button></td>";
                    cadastros += "</tr>";

                })

                $('#bodyTable').html(cadastros);
            },
            error: function(error){
                console.log(error);
            }

        });
    }

    function result_dml(){

        $.ajax('/view/exec-03.php', {

            method: 'GET',
            dataType: 'JSON',
            beforeSend: function(){
                $('#theadDml').html('');
                $('#bodyDml').html('');
            },
            success: function(response){

                var theadDml = '<tr><th>usuario</th><th>maior_50_anos</th></tr>';
                var bodyDml;

                $.each(response, function(index, value){
                    bodyDml += '<tr>';
                    bodyDml += '<td>' + value.usuario + '</td>';
                    bodyDml += '<td>' + value.maior_50_anos + '</td>';
                    bodyDml += '</tr>';
                })


                $('#theadDml').html(theadDml);
                $('#bodyDml').html(bodyDml);

            },
            error: function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Execute os scripts para funcionar corretamente!'
                });
            }

        });
    }

    $('body').on('click', "#btnCadastrar", function(){

        data = {
            'function': 'insert',
            'name': $("input[name='name']").val(),
            'userName': $("input[name='userName']").val(),
            'zipCode': $("input[name='zipCode']").val(),
            'email': $("input[name='email']").val(),
            'password': $("input[name='password']").val(),
        }


        Swal.fire({
            title: 'Os dados estão corretos?',
            text: "A ação de cadastrar alguma informação incorreta, pode gerar complicações no cadastro",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Cadastrar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax('/view/exec-01.php', {

                    method: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function(response){

                        console.log(response);

                        Swal.fire({
                            icon: 'success',
                            title: 'Cadastro incluído!',
                            text: 'O registro de '+$("input[name='name']").val()+' foi incluído com sucesso e esta identificado pelo ID '+ response +'',
                            showConfirmButton: true,
                            confirmButtonText: 'Ok',
                        });
                        limpaForm();
                        get_cadastros();
                    },
                    error: function(error){

                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: error.responseText
                        });
                    }

                });


            }
        })


    })

    $('body').on('click', '#btnDelete', function(){

        data = {
            'function': 'delete',
            'id': $(this).data('id')
        }

        $.ajax('/view/exec-01.php', {

            method: 'POST',
            data: data,
            dataType: 'JSON',
            success: function(response){
                if(response == '1'){

                    Swal.fire({
                        icon: 'success',
                        title: 'Cadastro excluído com sucesso!',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    get_cadastros();
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Houve um erro ao excluir o cadastro!'
                    });
                }
            }

        });

    });

    $('body').on('click', '#btnAtualizarTabela', function(){
        get_cadastros();
    });

    get_cadastros();
    result_dml();
});
