<?php
$title = "Teste Superlógica";
include 'view/includes/head.php';
?>

<div class="row">
    <div class="col-md-12">
        <header>
            Etapa Teste Superlógica - Bruno Diniz
            <br>
            <br>
            <span> Acesse o arquivo de configuração em <b>config/Config.php</b> e configure os dados do banco de dados</span>
            <br>
            <span> Em seguida execute os scripts em <b>database/script.sql</b></span>
        </header>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-4">
        <a class="btn btn-primary" href="/view/exercicio-01.php" role="button">Exercício 01</a>
        <a class="btn btn-primary" href="/view/exec-02.php" role="button">Exercício 02</a>
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" role="button">Exercício 03</a>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="collapse" id="collapseExample">
            <div class="well">

                <h5>Para funcionar da forma correta, execute o script localizado em <b>database/script.sql</b><br></h5>

                <table class="table table-striped center-header table table-bordered dt-responsive">
                    <thead id="theadDml">
                    </thead>
                    <tbody id="bodyDml">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
include 'view/includes/footer.php';
?>
